package com.nimbusds.srp6;


import java.math.BigInteger;


/**
 * Custom routine interface for computing the password key 'k'. Calculation of
 * this value is required by the public server value 'b'
 * {@link SRP6ServerSession server session} as well as by the client session
 * shared key 'S' {@link SRP6ClientSession client session}.
 * 
 * <p>
 * If you don't want to employ the {@link SRP6Routines#computeK default routine}
 * for computing 'k' you can use this interface to define your own. Remember to
 * make sure that exactly the same routine is used by both the client and the
 * session sessionn else authentication will fail.
 * 
 * @author Simon Massey
 */
public interface KRoutine {

	/**
	 * Computes the SRP-6 multiplier k
	 * 
	 * @param cryptoParams
	 *            The crypto parameters for the SRP-6a protocol.
	 * @param ctx
	 *            Snapshot of the SRP-6a client session variables which may be
	 *            used in the computation of the multiplier 'k'.
	 * 
	 * @return k The multiple used in both server 'B' and client 'S'
	 */
	public BigInteger computeK(final SRP6CryptoParams cryptoParams, final KRoutineContext ctx);
}
