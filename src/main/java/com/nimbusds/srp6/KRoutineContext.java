package com.nimbusds.srp6;

import java.math.BigInteger;

public class KRoutineContext {

	public final BigInteger N;
	public final BigInteger g;

	public KRoutineContext(BigInteger N, BigInteger g) {
		this.N = N;
		this.g = g;
	}

}
